import axios from "axios";
import { getToken, logout } from "utils/utils";

export const getAutores = async () => {
  const token = getToken();
  return axios({
    method: "GET",
    url: `${process.env.REACT_APP_API_URL}/api/autor/ListaTodos`,
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    },
  }).catch((e) => {
    const { status } = e.response;
    if (status === 401) {
      logout();
    } else {
      throw Error(e.response.data.erros);
    }
  });
};

export const saveAutor = async (data) => {
  const token = getToken();
  return axios({
    method: "POST",
    data,
    url: `${process.env.REACT_APP_API_URL}/api/autor/CrearAutor`,
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    },
  }).catch((e) => {
    const { status } = e.response;
    console.log(e.response);
    if (status === 401) {
      logout();
    } else {
      throw Error(e.response.data.erros);
    }
  });
};