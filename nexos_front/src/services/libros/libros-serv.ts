import axios from "axios";
import { getToken, logout } from "utils/utils";

export const getLibros = async () => {
  const token = getToken();
  return axios({
    method: "GET",
    url: `${process.env.REACT_APP_API_URL}/api/libro/ListaTodos`,
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    },
  }).catch((e) => {
    const { status } = e.response;
    if (status === 401) {
      logout();
    } else {
      throw Error(e.response.data.erros);
    }
  });
};

export const saveLibro = async (data) => {
  const token = getToken();
  return axios({
    method: "POST",
    data,
    url: `${process.env.REACT_APP_API_URL}/api/libro/CrearLibro`,
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    },
  }).catch((e) => {
    const { status } = e.response;
    console.log(e.response);
    if (status === 401) {
      logout();
    } else {
      throw Error(e.response.data.erros);
    }
  });
};