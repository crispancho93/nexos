export interface ISession {
    token: string
    userName: string
}