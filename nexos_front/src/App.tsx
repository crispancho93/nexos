import './App.css';
import 'antd/dist/antd.min.css';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import useToken from './hooks/use-token';
import PrivateRoute from './routes/private-route';
import {
  Login,
  NotFound,
  Shared,
  Libros,
  Autores
} from './components/index';

const App = () => {
  const { session } = useToken();
  return (
    <Router>
      <Routes>
        <Route path='/' element={<Login />} />
        <Route element={<PrivateRoute isLogged={session} />} >

          <Route path='/libros' element={<Shared component={Libros} />} />
          <Route path='/autores' element={<Shared component={Autores} />} />
          
          <Route path='*' element={<NotFound />} />
        </Route>
      </Routes>
    </Router>
  )
}

export default App;