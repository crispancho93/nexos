import { Modal, Form, Row, Col, Select, Input } from "antd";
import { saveAutor } from "services/autores/autor-serv";
import { toast } from "utils/utils";

const Nuevo = ({ open, setOpen, loadData }) => {
  const [form] = Form.useForm();

  const handleSubmit = async (item: any) => {
    console.log(item);
    saveAutor(item)
      .then((d: any) => {
        toast(
          "success",
          process.env.REACT_APP_APP_NAME,
          "Guardado correctamente"
        );
        form.resetFields();
        setOpen(false);
        loadData();
      })
      .catch((e) => {
        console.log(e.message);
        toast("error", process.env.REACT_APP_APP_NAME, e.message);
      });
  };

  return (
    <Modal
      title="Nuevo autor"
      centered
      open={open}
      onOk={form.submit}
      onCancel={() => setOpen(false)}
      width={1000}
    >
      <Form onFinish={handleSubmit} form={form} autoComplete="off">
        <Row gutter={24}>
          <Col xs={24} sm={6} md={6}>
            <Form.Item
              label={"Nombre"}
              labelCol={{ span: 24 }}
              name="NombreCompleto"
              rules={[{ required: true, message: "¡Valor obligatorio!" }]}
            >
              <Input placeholder="Nombre" />
            </Form.Item>
          </Col>

          <Col xs={24} sm={6} md={6}>
            <Form.Item
              label={"Fecha nacimiento"}
              labelCol={{ span: 24 }}
              name="FechaNacimiento"
              rules={[{ required: false, message: "¡Valor obligatorio!" }]}
            >
              <Input type="date" placeholder="Fecha nacimiento" />
            </Form.Item>
          </Col>

          <Col xs={24} sm={6} md={6}>
            <Form.Item
              label={"Ciudad"}
              labelCol={{ span: 24 }}
              name="Ciudad"
              rules={[{ required: true, message: "¡Valor obligatorio!" }]}
            >
              <Input placeholder="Ciudad" />
            </Form.Item>
          </Col>

          <Col xs={24} sm={6} md={6}>
            <Form.Item
              label={"Correo"}
              labelCol={{ span: 24 }}
              name="Correo"
              rules={[{ required: true, message: "¡Valor obligatorio!" }]}
            >
              <Input type="email" placeholder="Correo" />
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </Modal>
  );
};

export default Nuevo;
