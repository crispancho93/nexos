import React, { useEffect, useState } from "react";
import { Button, Input, Table, Tag, Row, Col } from "antd";
import { PlusSquareOutlined } from "@ant-design/icons";
import { getAutores } from "services/autores/autor-serv";
import Nuevo from "./nuevo";

const { Search } = Input;

const Libros = () => {
  const [data, setData] = useState([]);
  const [open, setOpen] = useState(false);
  const [filter, setFilter] = useState("");

  const loadData = () => {
    getAutores().then((d: any) => {
      const data = d.data;
      setData([...data]);
    });
  };

  useEffect(() => {
    loadData();
  }, []);

  const filterValue = (value) => {
    setFilter(value);
  };

  const columns = [
    {
      title: "Nombre",
      dataIndex: "NombreCompleto",
      filteredValue: [filter],
      onFilter: (value: string, record: any) => {
        return (
          String(record.NombreCompleto)
            .toLocaleLowerCase()
            .includes(value.toLowerCase()) ||
          String(record.Ciudad)
            .toLocaleLowerCase()
            .includes(value.toLowerCase()) ||
          String(record.Correo)
            .toLocaleLowerCase()
            .includes(value.toLowerCase())
        );
      },
    },
    {
      title: "Fecha nacimiento",
      dataIndex: "FechaNacimiento",
      render: (value) => {
        return value.split('T')[0];
      }
    },
    {
      title: "Ciudad",
      dataIndex: "Ciudad",
    },
    {
      title: "Correo",
      dataIndex: "Correo",
      render: (_, { Correo }) => (
        <Tag color="cyan" key={Correo}>
          {Correo}
        </Tag>
      ),
    },
  ];

  return (
    <div style={{ marginTop: 10 }}>
      <h3>Autores</h3>
      <div style={{ marginBottom: 10, marginTop: 10 }}>
        <Row gutter={24}>
          <Col xs={12} sm={12} md={12}>
            <Search placeholder="Buscar..." onSearch={filterValue} />
          </Col>

          <Col xs={12} sm={12} md={12}>
            <div style={{ float: "right" }}>
              <Button onClick={() => setOpen(true)}>
                <PlusSquareOutlined />
              </Button>
            </div>
          </Col>
        </Row>
      </div>

      <Table
        rowKey={"Id"}
        columns={columns}
        dataSource={data}
        size="small"
        scroll={{
          x: "100%",
        }}
      />

      <Nuevo open={open} setOpen={setOpen} loadData={loadData} />
    </div>
  );
};

export default Libros;
