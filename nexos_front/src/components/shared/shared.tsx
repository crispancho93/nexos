import { Link } from "react-router-dom";
import { Menu, Layout, Row, Col } from "antd";
import { BookOutlined, UserOutlined, LogoutOutlined } from "@ant-design/icons";
import { logout } from "utils/utils";

const { Content, Footer } = Layout;

const Shared = ({ component: Component }: any) => {
  return (
    <Layout style={{ minHeight: "100vh" }}>
      <Menu mode="horizontal" defaultSelectedKeys={["Libros"]}>
        <Menu.Item key="Libros" icon={<BookOutlined />}>
          <Link to="/libros">Libros</Link>
        </Menu.Item>
        <Menu.Item key="Autores" icon={<UserOutlined />}>
          <Link to="/autores">Autores</Link>
        </Menu.Item>
        <Menu.Item
          key="Logout"
          style={{ marginLeft: "auto" }}
          icon={<LogoutOutlined />}
          onClick={logout}
        >
          Logout
        </Menu.Item>
      </Menu>

      <Layout className="site-layout">
        <Content style={{ margin: "0 16px" }} className="myContent">
          <Row>
            <Col span={24} style={{ padding: 10 }}>
              <Component />
            </Col>
          </Row>
        </Content>

        <Footer style={{ textAlign: "center" }}>
          Copyright Nexos Software - APP 7.1.1
        </Footer>
      </Layout>
    </Layout>
  );
};

export default Shared;
