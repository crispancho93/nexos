import { Modal, Form, Row, Col, Select, Input } from "antd";
import React, { useState, useEffect } from "react";
import { getAutores } from "services/autores/autor-serv";
import { saveLibro } from "services/libros/libros-serv";
import { toast } from "utils/utils";

const { Option } = Select;

const Nuevo = ({ open, setOpen, loadData }) => {
  const [form] = Form.useForm();
  const [autores, setAutores] = useState([]);

  useEffect(() => {
    getAutores().then((d: any) => {
      const data = d.data;
      setAutores([...data]);
    });
  }, []);

  const handleSubmit = async (item: any) => {
    console.log(item);
    saveLibro(item)
      .then((d: any) => {
        toast(
          "success",
          process.env.REACT_APP_APP_NAME,
          "Guardado correctamente"
        );
        form.resetFields();
        setOpen(false);
        loadData();
      })
      .catch((e) => {
        console.log(e.message);
        toast("error", process.env.REACT_APP_APP_NAME, e.message);
      });
  };

  return (
    <Modal
      title="Nuevo libro"
      centered
      open={open}
      onOk={form.submit}
      onCancel={() => setOpen(false)}
      width={1000}
    >
      <Form onFinish={handleSubmit} form={form} autoComplete="off">
        <Row gutter={24}>
          <Col xs={24} sm={6} md={6}>
            <Form.Item
              name="IdAutor"
              label="Autor"
              labelCol={{ span: 24 }}
              rules={[{ required: true, message: "¡Valor obligatorio!" }]}
            >
              <Select
                showSearch
                allowClear
                optionFilterProp="children"
                placeholder="Autor"
              >
                {autores.map((d) => (
                  <Option key={d.Id} value={d.Id}>
                    {d.NombreCompleto}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </Col>

          <Col xs={24} sm={6} md={6}>
            <Form.Item
              label={"Título"}
              labelCol={{ span: 24 }}
              name="Titulo"
              rules={[{ required: true, message: "¡Valor obligatorio!" }]}
            >
              <Input placeholder="Título" />
            </Form.Item>
          </Col>

          <Col xs={24} sm={6} md={6}>
            <Form.Item
              label={"Año"}
              labelCol={{ span: 24 }}
              name="Anio"
              rules={[{ required: true, message: "¡Valor obligatorio!" }]}
            >
              <Input type="number" placeholder="Año" />
            </Form.Item>
          </Col>

          <Col xs={24} sm={6} md={6}>
            <Form.Item
              label={"Género"}
              labelCol={{ span: 24 }}
              name="Genero"
              rules={[{ required: true, message: "¡Valor obligatorio!" }]}
            >
              <Input placeholder="Género" />
            </Form.Item>
          </Col>

          <Col xs={24} sm={6} md={6}>
            <Form.Item
              label={"Páginas"}
              labelCol={{ span: 24 }}
              name="NumeroPaginas"
              rules={[{ required: false, message: "¡Valor obligatorio!" }]}
            >
              <Input type="number" placeholder="Páginas" />
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </Modal>
  );
};

export default Nuevo;
