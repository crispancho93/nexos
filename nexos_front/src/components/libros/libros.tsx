import React, { useEffect, useState } from "react";
import { Button, Input, Table, Tag, Row, Col } from "antd";
import { PlusSquareOutlined } from "@ant-design/icons";
import { getLibros } from "services/libros/libros-serv";
import Nuevo from "./nuevo";

const { Search } = Input;

const Libros = () => {
  const [data, setData] = useState([]);
  const [open, setOpen] = useState(false);
  const [filter, setFilter] = useState("");

  const loadData = () => {
    getLibros().then((d: any) => {
      const data = d.data;
      setData([...data]);
    });
  };

  useEffect(() => {
    loadData();
  }, []);

  const filterValue = (value) => {
    setFilter(value);
  };

  const columns = [
    {
      title: "Autor",
      dataIndex: "NomnbreAutor",
      filteredValue: [filter],
      onFilter: (value: string, record: any) => {
        return (
          String(record.NomnbreAutor)
            .toLocaleLowerCase()
            .includes(value.toLowerCase()) ||
          String(record.Titulo)
            .toLocaleLowerCase()
            .includes(value.toLowerCase()) ||
          String(record.Anio)
            .toLocaleLowerCase()
            .includes(value.toLowerCase())
        );
      },
    },
    {
      title: "Título",
      dataIndex: "Titulo",
    },
    {
      title: "Año",
      dataIndex: "Anio",
    },
    {
      title: "Género",
      dataIndex: "Genero",
      render: (_, { Genero }) => (
        <Tag color="cyan" key={Genero}>
          {Genero}
        </Tag>
      ),
    },
    {
      title: "Páginas",
      dataIndex: "NumeroPaginas",
    },
  ];

  return (
    <div style={{ marginTop: 10 }}>
      <h3>Libros</h3>
      <div style={{ marginBottom: 10, marginTop: 10 }}>
        <Row gutter={24}>
          <Col xs={12} sm={12} md={12}>
            <Search placeholder="Buscar..." onSearch={filterValue} />
          </Col>

          <Col xs={12} sm={12} md={12}>
            <div style={{ float: "right" }}>
              <Button onClick={() => setOpen(true)}>
                <PlusSquareOutlined />
              </Button>
            </div>
          </Col>
        </Row>
      </div>

      <Table
        rowKey={"Id"}
        columns={columns}
        dataSource={data}
        size="small"
        scroll={{
          x: "100%",
        }}
      />

      <Nuevo open={open} setOpen={setOpen} loadData={loadData} />
    </div>
  );
};

export default Libros;
