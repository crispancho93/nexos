import { useNavigate } from "react-router-dom";
import { Result, Button } from "antd";

const NotFound = () => {
  const navigate = useNavigate();
  const back = () => {
    navigate(-1);
  };
  return (
    <Result
      status="404"
      title="404"
      subTitle="Sorry, the page you visited does not exist."
      extra={
        <Button type="primary" onClick={back}>
          Back Home
        </Button>
      }
    />
  );
};

export default NotFound;
