﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace nexos_api.Entities
{
    [Table("Libros")]
    public class LibroModel
    {
        [Key]
        public int Id { get; set; }
        public int IdAutor { get; set; }
        public string? Titulo { get; set; }
        public int Anio { get; set; }
        public string? Genero { get; set; }
        public int NumeroPaginas { get; set; }
    }
}
