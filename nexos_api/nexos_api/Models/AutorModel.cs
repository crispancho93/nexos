﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace nexos_api.Entities
{
    [Table("Autor")]
    public class AutorModel
    {
        [Key]
        public int Id { get; set; }
        public string? NombreCompleto { get; set; }
        public DateTime? FechaNacimiento { get; set; }
        public string? Ciudad { get; set; }
        public string? Correo { get; set; }
    }
}
