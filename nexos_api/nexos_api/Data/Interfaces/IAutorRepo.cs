﻿using nexos_api.Entities;

namespace nexos_api.Data.Interfaces
{
    public interface IAutorRepo
    {
        public Task<int> Create(AutorModel autor);
        public Task<IEnumerable<AutorModel>> ListaTodos();
        public Task<AutorModel?> ListaPorId(int id);
    }
}
