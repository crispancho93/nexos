﻿using nexos_api.Dtos;
using nexos_api.Entities;

namespace nexos_api.Data.Interfaces
{
    public interface ILibroRepo
    {
        public Task<int> Create(LibroModel libro);
        public IEnumerable<LibroDto> ListaTodos();
    }
}
