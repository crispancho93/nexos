﻿using Microsoft.EntityFrameworkCore;
using nexos_api.Data.Interfaces;
using nexos_api.Entities;

namespace nexos_api.Data
{
    public class AutorRepo : IAutorRepo
    {
        private readonly AppDbContext _context;
        public AutorRepo(AppDbContext context) => _context = context;

        public async Task<int> Create(AutorModel autor)
        {
            _context.Autores.Add(autor);
            await _context.SaveChangesAsync();
            return autor.Id;
        }

        public async Task<IEnumerable<AutorModel>> ListaTodos()
        {
            var autores = await _context.Autores.ToListAsync();
            return autores;
        }

        public async Task<AutorModel?> ListaPorId(int id)
        {
            var autor = await _context.Autores.FindAsync(id);
            return autor;
        }
    }
}
