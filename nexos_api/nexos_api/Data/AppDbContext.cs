﻿using Microsoft.EntityFrameworkCore;
using nexos_api.Entities;

namespace nexos_api.Data
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> opt) : base(opt) { }

        public DbSet<LibroModel> Libros { get; set; }
        public DbSet<AutorModel> Autores { get; set; }
    }

}
