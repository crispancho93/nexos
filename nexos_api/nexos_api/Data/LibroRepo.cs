﻿using Microsoft.EntityFrameworkCore;
using nexos_api.Data.Interfaces;
using nexos_api.Dtos;
using nexos_api.Entities;

namespace nexos_api.Data
{
    public class LibroRepo : ILibroRepo
    {
        private readonly AppDbContext _context;
        public LibroRepo(AppDbContext context) => _context = context;

        public async Task<int> Create(LibroModel libro)
        {
            _context.Libros.Add(libro);
            await _context.SaveChangesAsync();
            return libro.Id;
        }

        public IEnumerable<LibroDto> ListaTodos()
        {

            var data = from a in _context.Libros
                    join b in _context.Autores on a.IdAutor equals b.Id
                    select new LibroDto
                    {
                        Id = a.Id,
                        Anio = a.Anio,
                        IdAutor = a.IdAutor,
                        Genero = a.Genero,
                        NumeroPaginas = a.NumeroPaginas,
                        Titulo = a.Titulo,
                        NomnbreAutor = b.NombreCompleto

                    };
            
            return data;
        }
    }
}
