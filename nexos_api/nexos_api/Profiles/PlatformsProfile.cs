﻿using AutoMapper;
using nexos_api.Dtos;
using nexos_api.Entities;

namespace nexos_api.Profiles
{
    public class PlatformsProfile : Profile
    {
        public PlatformsProfile()
        {
            // Source -> Target
            CreateMap<AutorModel, AutorDto>();
            CreateMap<AutorDto, AutorModel>();

            CreateMap<LibroModel, LibroDto>();
            CreateMap<LibroDto, LibroModel>();
        }
    }
}
