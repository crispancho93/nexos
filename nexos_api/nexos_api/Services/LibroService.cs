﻿using AutoMapper;
using nexos_api.Data.Interfaces;
using nexos_api.Dtos;
using nexos_api.Entities;
using nexos_api.Services.Interfaces;
using nexos_api.Services.Validators;

namespace nexos_api.Services
{
    public class LibroService : ILibroService
    {
        private readonly IMapper _mapper;
        private readonly ILibroRepo _libroRepo;
        private readonly IAutorRepo _autorRepo;

        public LibroService(IMapper mapper, ILibroRepo libroRepo, IAutorRepo autorRepo) 
            => (_mapper, _libroRepo, _autorRepo) = (mapper, libroRepo, autorRepo);

        public async Task<int> CrearLibro(LibroDto LibroDto)
        {
            var libroModel = _mapper.Map<LibroModel>(LibroDto);
            var libroValidator = new LibroValidator(_libroRepo, _autorRepo).Validate(libroModel);

            if (!libroValidator.IsValid)
            {
                throw new Exception(string.Join(", ", libroValidator.Errors.Select(e => e.ErrorMessage)));
            }

            var insertedId = await _libroRepo.Create(libroModel);

            if (insertedId is 0) throw new Exception("No se pudo guardar el autor");
            return insertedId;
        }

        public IEnumerable<LibroDto> ListaTodos()
        {
            var libros = _libroRepo.ListaTodos();
            return _mapper.Map<IEnumerable<LibroDto>>(libros);
        }
    }
}
