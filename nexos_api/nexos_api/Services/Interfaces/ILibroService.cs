﻿using nexos_api.Dtos;

namespace nexos_api.Services.Interfaces
{
    public interface ILibroService
    {
        public Task<int> CrearLibro(LibroDto LibroDto);
        public IEnumerable<LibroDto> ListaTodos();
    }
}
