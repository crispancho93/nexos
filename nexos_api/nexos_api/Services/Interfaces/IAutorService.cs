﻿using nexos_api.Dtos;

namespace nexos_api.Services.Interfaces
{
    public interface IAutorService
    {
        public Task<int> CrearAutor(AutorDto autorDto);
        public Task<IEnumerable<AutorDto>> ListaTodos();
    }
}
