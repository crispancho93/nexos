﻿using AutoMapper;
using nexos_api.Data.Interfaces;
using nexos_api.Dtos;
using nexos_api.Entities;
using nexos_api.Services.Interfaces;
using nexos_api.Services.Validators;

namespace nexos_api.Services
{
    public class AutorService : IAutorService
    {
        private readonly IMapper _mapper;
        private readonly IAutorRepo _autorRepo;
        public AutorService(IMapper mapper, IAutorRepo autorRepo) => (_mapper, _autorRepo) = (mapper, autorRepo);

        public async Task<int> CrearAutor(AutorDto autorDto)
        {
            var autorModel = _mapper.Map<AutorModel>(autorDto);
            var autorValidator = new AutorValidator().Validate(autorModel);

            if (!autorValidator.IsValid)
            {
                throw new Exception(string.Join(", ", autorValidator.Errors.Select(e => e.ErrorMessage)));
            }

            var insertedId = await _autorRepo.Create(autorModel);

            if (insertedId is 0) throw new Exception("No se pudo guardar el autor");
            return insertedId;
        }

        public async Task<IEnumerable<AutorDto>> ListaTodos()
        {
            var autores = await _autorRepo.ListaTodos();
            return _mapper.Map<IEnumerable<AutorDto>>(autores);
        }
    }
}
