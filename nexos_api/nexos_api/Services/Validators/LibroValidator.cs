﻿using FluentValidation;
using nexos_api.Data.Interfaces;
using nexos_api.Entities;

namespace nexos_api.Services.Validators
{
    public class LibroValidator : AbstractValidator<LibroModel>
    {
        public LibroValidator(ILibroRepo libroRepo, IAutorRepo autorRepo)
        {
            RuleFor(x => x.IdAutor).NotNull().WithMessage("El {PropertyName} es obligatorio");
            RuleFor(x => x.IdAutor).GreaterThan(0).WithMessage("El {PropertyName} debe ser mayor a cero");
            
            RuleFor(x => x.Titulo).NotEmpty().WithMessage("El {PropertyName} es obligatorio");
            RuleFor(x => x.Titulo).MaximumLength(45).WithMessage("El {PropertyName} debe tener maxímo {MaxLength} caracteres");

            RuleFor(x => x.Anio).LessThanOrEqualTo(DateTime.Now.Year).WithMessage("El {PropertyName} no puede ser mayor al año actual");

            RuleFor(x => x.Genero).NotEmpty().WithMessage("El {PropertyName} es obligatorio");
            RuleFor(x => x.Genero).MaximumLength(45).WithMessage("El {PropertyName} debe tener maxímo {MaxLength} caracteres");

            RuleFor(x => x.NumeroPaginas).LessThanOrEqualTo(10000).WithMessage("El {PropertyName} no puede ser mayor al 10.000");

            RuleFor(x => x).Custom((x, context) =>
            {
                var count = libroRepo.ListaTodos().Count();
                if (count >= 10)
                    context.AddFailure("No es posible registrar el libro, se alcanzó el máximo permitido");
            });

            RuleFor(x => x).Custom((x, context) =>
            {
                var find = autorRepo.ListaPorId(x.IdAutor).Result;
                if (find is null)
                    context.AddFailure("El autor no está registrado");
            });
        }
    }
}
