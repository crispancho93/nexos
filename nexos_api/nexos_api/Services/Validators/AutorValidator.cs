﻿using FluentValidation;
using nexos_api.Entities;

namespace nexos_api.Services.Validators
{
    public class AutorValidator : AbstractValidator<AutorModel>
    {
        public AutorValidator()
        {
            RuleFor(x => x.NombreCompleto).NotEmpty().WithMessage("El {PropertyName} completo es obligatorio");
            RuleFor(x => x.NombreCompleto).MaximumLength(45).WithMessage("El {PropertyName} completo debe tener maxímo {MaxLength} caracteres");

            RuleFor(x => x.Correo).NotEmpty().WithMessage("El {PropertyName} es obligatorio");
            RuleFor(x => x.Correo).MaximumLength(45).WithMessage("El {PropertyName} debe tener maxímo {MaxLength} caracteres");
            RuleFor(x => x.Correo).EmailAddress().WithMessage("El {PropertyName} no tiene el formato correcto");

            RuleFor(x => x.Ciudad).NotEmpty().WithMessage("La {PropertyName} es obligatorio");
            RuleFor(x => x.Ciudad).MaximumLength(45).WithMessage("La {PropertyName} debe tener maxímo {MaxLength} caracteres");

        }
    }
}
