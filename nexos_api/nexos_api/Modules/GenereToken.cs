using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using System.Text;
using nexos_api.Modules.Interfaces;

namespace nexos_api.Modules
{
    class GenereToken : IGenereToken
    {
        private readonly IConfiguration _config;

        public GenereToken(IConfiguration config) => _config = config;

        public string Create(string UserName)
        {
            var claims = new[]
            {
            new Claim(ClaimTypes.NameIdentifier, UserName)
        };
            var token = new JwtSecurityToken
            (
                issuer: _config["Jwt:Issuer"],
                audience: _config["Jwt:Audience"],
                claims: claims,
                expires: DateTime.UtcNow.AddDays(1),
                notBefore: DateTime.UtcNow,
                signingCredentials: new SigningCredentials(
                    new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"])),
                    SecurityAlgorithms.HmacSha256
                )
            );
            var tokenString = new JwtSecurityTokenHandler().WriteToken(token);
            return tokenString;
        }
    }
}