namespace nexos_api.Modules.Interfaces;

public interface IGenereToken
{
    public string Create(string UserName);
}