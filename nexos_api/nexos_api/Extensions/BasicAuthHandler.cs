using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text.Encodings.Web;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Options;
using System.Text;

namespace nexos_api.Extensions
{
    public class BasicAuthHandler : AuthenticationHandler<AuthenticationSchemeOptions>
    {
        private readonly IConfiguration _config;

        public BasicAuthHandler(IOptionsMonitor<AuthenticationSchemeOptions> options,
            ILoggerFactory logger,
            UrlEncoder encoder,
            ISystemClock clock,
            IConfiguration config
        ) : base(options, logger, encoder, clock)
        {
            _config = config;
        }

        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            bool result = false;

            if (!Request.Headers.ContainsKey("Authorization"))
            {
                return AuthenticateResult.Fail("Invalid Header");
            }

            try
            {
                var autHeader = AuthenticationHeaderValue.Parse(Request.Headers["Authorization"]);
                var credentialsBytes = Convert.FromBase64String(autHeader.Parameter ?? "");
                var credentials = Encoding.UTF8.GetString(credentialsBytes).Split(new[] { ':' }, 2);
                var user = credentials[0];
                var pwd = credentials[1];
                result = false;

            }
            catch
            {
                return AuthenticateResult.Fail("Invalid Header");
            }

            if (!result)
                return AuthenticateResult.Fail("Invalid credentials");

            var claims = new Claim[]
            {
            new Claim(ClaimTypes.NameIdentifier, "id"),
            new Claim(ClaimTypes.Name, "user")
            };

            var identity = new ClaimsIdentity(claims, Scheme.Name);
            var principal = new ClaimsPrincipal(identity);
            var ticket = new AuthenticationTicket(principal, Scheme.Name);
            return AuthenticateResult.Success(ticket);
        }
    }
}
