﻿using nexos_api.Data.Interfaces;
using nexos_api.Data;
using nexos_api.Services.Interfaces;
using nexos_api.Services;
using nexos_api.Modules.Interfaces;
using nexos_api.Modules;

namespace nexos_api.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static void RegisterServices(this IServiceCollection services)
        {
            services.AddScoped<IAutorRepo, AutorRepo>();
            services.AddScoped<ILibroRepo, LibroRepo>();
            services.AddScoped<IAutorService, AutorService>();
            services.AddScoped<ILibroService, LibroService>();
            services.AddScoped<IGenereToken, GenereToken>();
        }
    }
}
