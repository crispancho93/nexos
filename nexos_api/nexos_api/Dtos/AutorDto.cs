﻿using System.Text.Json.Serialization;

namespace nexos_api.Dtos
{
    public class AutorDto
    {
        [JsonPropertyName("Id")]
        public int Id { get; set; }

        [JsonPropertyName("NombreCompleto")]
        public string? NombreCompleto { get; set; }

        [JsonPropertyName("FechaNacimiento")]
        public DateTime? FechaNacimiento { get; set; }

        [JsonPropertyName("Ciudad")]
        public string? Ciudad { get; set; } 

        [JsonPropertyName("Correo")]
        public string? Correo { get; set; }
    }
}
