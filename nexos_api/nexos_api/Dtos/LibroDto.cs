﻿using System.Text.Json.Serialization;

namespace nexos_api.Dtos
{
    public class LibroDto
    {
        [JsonPropertyName("Id")]
        public int Id { get; set; }

        [JsonPropertyName("IdAutor")]
        public int IdAutor { get; set; }

        [JsonPropertyName("NomnbreAutor")]
        public string? NomnbreAutor { get; set; }

        [JsonPropertyName("Titulo")]
        public string? Titulo { get; set; }
        [JsonPropertyName("Anio")]

        public int Anio { get; set; }
        [JsonPropertyName("Genero")]
        public string? Genero { get; set; }

        [JsonPropertyName("NumeroPaginas")]
        public int NumeroPaginas { get; set; }
    }
}
