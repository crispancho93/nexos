using System.ComponentModel.DataAnnotations;

namespace nexos_api.Dtos
{
    public class UserDto
    {
        [Required(ErrorMessage = "El UserName es obligatorio")]
        public string? UserName { get; set; }
        [Required(ErrorMessage = "El Password es obligatorio")]
        public string? Password { get; set; }
    }
}