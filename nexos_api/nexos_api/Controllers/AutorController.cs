﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using nexos_api.Dtos;
using nexos_api.Services.Interfaces;

namespace nexos_api.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class AutorController : ControllerBase
    {
        private readonly IAutorService _autorService;
        public AutorController(IAutorService autorService) => _autorService = autorService;

        [HttpPost]
        [Route("CrearAutor")]
        public async Task<ActionResult> CrearAutor(AutorDto autorDto)
        {
            try
            {
                var insertedId = await _autorService.CrearAutor(autorDto);
                return Ok(new { insertedId });
            }
            catch (Exception ex)
            {
                return BadRequest(new { erros = ex.Message });
            }
        }

        [HttpGet]
        [Route("ListaTodos")]
        public async Task<ActionResult> ListaTodos()
        {
            try
            {
                var autores = await _autorService.ListaTodos();
                return Ok(autores);
            }
            catch (Exception ex)
            {
                return BadRequest(new { erros = ex.Message });
            }
        }
    }
}
