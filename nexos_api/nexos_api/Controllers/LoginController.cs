using Microsoft.AspNetCore.Mvc;
using nexos_api.Dtos;
using nexos_api.Modules.Interfaces;

namespace nexos_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class LoginController : ControllerBase
    {
        private readonly IConfiguration _config;
        private readonly IGenereToken _token;

        public LoginController(IConfiguration config, IGenereToken token) => (_config, _token) = (config, token);

        [HttpPost("Authenticate")]
        public ActionResult Authenticate(UserDto user)
        {
            try
            {
                if (user.UserName == _config["Jwt:UserApi"] && user.Password == _config["Jwt:PassApi"])
                {
                    var data = new
                    {
                        user.UserName,
                        Token = _token.Create(user.UserName)
                    };
                    return Ok(data);
                } 
                else
                {
                    return Unauthorized(new { msg = "UserName o Password son incorrectos" });
                }
            }
            catch (Exception ex)
            {
                return Unauthorized(new { msg = ex.Message });
            }
        }
    }
}
