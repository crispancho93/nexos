﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using nexos_api.Dtos;
using nexos_api.Services.Interfaces;

namespace nexos_api.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class LibroController : ControllerBase
    {
        private readonly ILibroService _libroService;
        public LibroController(ILibroService libroService) => _libroService = libroService;

        [HttpPost]
        [Route("CrearLibro")]
        public async Task<ActionResult> CrearLibro(LibroDto libroDto)
        {
            try
            {
                var insertedId = await _libroService.CrearLibro(libroDto);
                return Ok(new { insertedId });
            }
            catch (Exception ex)
            {
                return BadRequest(new { erros = ex.Message });
            }
        }

        [HttpGet]
        [Route("ListaTodos")]
        public ActionResult ListaTodos()
        {
            try
            {
                var libros = _libroService.ListaTodos();
                return Ok(libros);
            }
            catch (Exception ex)
            {
                return BadRequest(new { erros = ex.Message });
            }
        }
    }
}
